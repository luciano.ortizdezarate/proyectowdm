import pandas as pd

#Funcion que retorna tabla con las rutas de la topologia a simular.
def leerArchivo(nombreTopologia):
    dfRutas = pd.read_csv(f"./Topologias/{nombreTopologia}.csv",header=None)
    dfRutas= dfRutas[0].str.split('\t', expand=True)
    return dfRutas

#Función que retorna el valor del número de nodos, cantidad de enlaces y números de usuarios de la red.
def extraerValoresNLM(rutas):
    N = int(rutas[3][1]) # Número de nodos de la red
    L = int(rutas[3][2]) # Número de enlaces unidireccionales
    M = N*(N-1) # Numero de usuarios
    return N,L,M

def loadTopology(self):
        fileAddress = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', filter="Image files (*.txt *.top)")
        if fileAddress[0] is not "":
            address = fileAddress[0]
            self.graph = loadGraph(address)
            self.updateTableHeaders(self.graph.edges(), self.fsusSpinBox.value())
            self.kspSpinBox.setMaximum(self.graph.getNodesCount() - 1)
            self.topologyTotalCapacity.setText(str(self.graph.getTotalCapacity()))
            self.topologyName.setText(self.graph.name)
            self.topologyUsers.setText(str(len(self.graph.nodes) ** 2 - len(self.graph.nodes)))